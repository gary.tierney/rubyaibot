require 'sqlite3'
require 'concurrent'

require './src/bot/brain/data_source.rb'
require './src/bot/brain/data_processor.rb'
require './src/bot/client.rb'

require './src/bot/irc/learning_plugin.rb'
require './src/bot/irc/random_sentence_plugin.rb'

module AIBot
  class Config
    attr_reader :data_sources, :servers, :db, :brain, :irc

    def initialize
      @irc = {}
      @servers = []
      @data_sources = []
      @brain = {}
      @db = {}
    end
  end

  class Main
    def initialize
      @config = Config.new
    end

    def configure
      yield @config
    end

    def start
      create_db unless File.exists?(@config.db[:filename])

      db_connection = SQLite3::Database.open(@config.db[:filename])

      memory_learn_buffer = AIBot::InMemoryDataSource.new
      core_plugins = [
          LearningPlugin.new(memory_learn_buffer),
          RandomSentencePlugin.new(MarkovChainGenerator.new(db_connection))
      ]

      @config.data_sources.push memory_learn_buffer

      data_processor = AIBot::DataProcessor.new(db_connection, @config.data_sources)
      timeout = @config.brain[:processing_timeout]
      delay = @config.brain[:processing_frequency]

      scheduled_proc_task = Concurrent::TimerTask.new(execution_interval: delay, timeout_interval: timeout) do
        data_processor.update
      end
      scheduled_proc_task.execute

      @config.irc[:plugins] = (@config.irc[:plugins] || []) + core_plugins

      irc_threads = []
      @config.servers.each do |server|
        thr = Thread.new {
          client = AIBot::Client.new(server[:host], server[:channels], @config.irc || {})
          client.start
        }
        irc_threads.push(thr)
      end

      irc_threads.each do |thr|
        thr.join
      end

    end

    private

    def create_db
      db_connection = SQLite3::Database.new(@config.db[:filename])
      db_connection.execute '
        CREATE TABLE markov_trigrams (
          first_value VARCHAR(255),
          second_value VARCHAR(255),
          third_value VARCHAR(255),
          is_at_start INTEGER(1),
          average_frequency INTEGER(5),
          average_context_length INTEGER(5)
        );
      '

      db_connection.execute 'CREATE INDEX markov_starter_trigrams_IDX ON markov_trigrams(is_at_start, first_value);'
      db_connection.execute 'CREATE INDEX markov_bigrams_IDX ON markov_trigrams(first_value, second_value);'
    end
  end

  @instance = Main.new

  def self.instance; @instance end
end
