module AIBot
  class Plugin
    @types = []

    def execute(m); raise 'SubclassResponsibility'; end

    def self.accept_types(types)
      if types.is_a? Array
        @types = types
      elsif types.is_a? Symbol
        @types = [types]
      end
    end

    def self.types
      @types
    end

    def types
      self.class.types
    end
  end
end
