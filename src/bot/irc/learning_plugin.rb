require './src/bot/irc/plugin.rb'

module AIBot
  class LearningPlugin < Plugin

    accept_types [:message]

    def initialize(memory_buffer)
      @memory_buffer = memory_buffer
    end

    def execute(m)
      @memory_buffer.add m.message
    end
  end
end