require './src/bot/irc/plugin.rb'

module AIBot
  class MarkovChainGenerator
    def initialize(db_conn)
      @db_conn = db_conn
    end

    def generate(seed, len)
      row = @db_conn.get_first_row 'SELECT * FROM markov_trigrams WHERE is_at_start AND first_value LIKE `:seed:`', :seed => seed
      puts row
    end
  end

  class RandomSentencePlugin < Plugin
    accept_types :message

    def initialize(chain_generator)
      @generator = chain_generator
    end

    def execute(m)
      if m.message.include? '!rand'
        words = m.message.split(' ')
        words = words.slice(1, words.length)

        @generator.generate(words.sample, 24)
      end
    end
  end
end