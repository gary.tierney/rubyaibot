require 'cinch'
require './src/bot/irc/learning_plugin.rb'

module AIBot
  class Client < Cinch::Bot

    def initialize(server, channels, options = {})
      plugins = options[:plugins] || []

      super() do
        configure do |c|
          c.nick = options[:nick] || 'sfx'
          c.server = server
          c.channels = channels
        end

        on :message do |m|
          plugins.each { |plugin|
            plugin.execute m unless (m.events & plugin.types).empty?
          }
        end
      end
    end

  end
end