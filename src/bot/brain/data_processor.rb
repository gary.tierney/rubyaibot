require 'concurrent'

module AIBot
  class DataProcessor
    attr_accessor :stopped

    def initialize(db_connection, data_sources)
      @db_connection = db_connection
      @data_sources = data_sources
      @processed_data = []
    end

    def poll
      until stopped
        update
      end
    end

    def update
      futures = []
      data_buffer = []

      @data_sources.each { |data_source|

        futures.push Concurrent::Future.execute {
          data_source.get_data
        } if data_source.available?
      }

      until futures.empty? do
        futures.each { |future|
          next unless future.fulfilled?

          futures.delete future
          data_buffer.concat future.value
        }
      end

      data_buffer.each { |data|
        process_data data
      }

      flush_processed_data_batch if @processed_data.length > 0
    end

    private

    SENTENCE_REGEXP = /([^.!?\s][^.!?]*(?:[.!?](?!['"]?\s|$)[^.!?]*)*[.!?]?['"]?(?=\s|$))+/

    def process_data(data)
      begin
        words = data.split(' ')
        words.each_index { |idx|
          next unless words[idx + 1] && words[idx + 2]

          first, second, third = words[idx], words[idx + 1], words[idx + 2]
          next if first.include?('.') || second.include?('.') || third.include?('.')

          @processed_data.push :first => first, :second => second, :third => third, :at_start => idx == 0 # @todo - make this parse sentences
        }
      rescue Exception => e
        puts e.to_s
      end
    end

    def flush_processed_data_batch
      begin
        sql = 'INSERT INTO markov_trigrams
          (first_value, second_value, third_value, is_at_start, average_frequency, average_context_length) VALUES
          (?, ?, ?, ?, 0, 0)'
        insert_stmt = @db_connection.prepare sql

        @db_connection.transaction
        until @processed_data.length == 0
          proc_data = @processed_data.pop

          insert_stmt.execute proc_data[:first], proc_data[:second], proc_data[:third], proc_data[:at_start] ? 1 : 0
        end
        @db_connection.commit
      rescue Exception => e
        @db_connection.rollback
        puts e.to_s
      end
    end
  end
end