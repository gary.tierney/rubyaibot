module AIBot
  class DataSource
    def available?; raise 'SubclassResponsibility' ; end
    def get_data; raise 'SubclassResponsibility' ; end
  end

  class InMemoryDataSource < DataSource
    def initialize
      @buffer = []
    end

    def available?
      @buffer.size > 0
    end

    def add(data)
      @buffer.push data
    end

    def get_data
      data = @buffer.dup
      @buffer = []

      data
    end
  end
end