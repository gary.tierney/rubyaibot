AIBot.instance.configure do |config|
  config.brain[:processing_timeout] = 30
  config.brain[:processing_frequency] = 3
end