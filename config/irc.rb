require './src/bot/irc/learning_plugin.rb'

AIBot.instance.configure do |config|
  config.servers.push :host => 'irc.freenode.net', :channels => %w(#rs)
  config.irc[:plugins] = []
end