AIBot.instance.configure do |config|
  config.db[:driver] = 'sqlite3' # Only sqlite3 is supported atm, may consider using a dbal
  config.db[:filename] = 'brain.sqlite3'
end